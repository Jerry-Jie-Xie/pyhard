========
Modules
========

Base
===================

.. automodule:: pyhard.base
   :members:
   :undoc-members:
   :show-inheritance:

Classification
===================

.. automodule:: pyhard.classification
   :members:
   :undoc-members:
   :show-inheritance:

CLI
===================

.. automodule:: pyhard.cli
   :members:
   :show-inheritance:

Context
===================

.. automodule:: pyhard.context
   :members:
   :show-inheritance:

Feature Selection
===================

.. automodule:: pyhard.feature_selection
   :members:
   :undoc-members:
   :show-inheritance:

Hyperparameter Optimization
=============================

.. automodule:: pyhard.hpo
   :members:
   :show-inheritance:

Integrator
===================

.. automodule:: pyhard.integrator
   :members:
   :undoc-members:
   :show-inheritance:

Hardness Measures
===================

.. automodule:: pyhard.measures
   :members:
   :show-inheritance:

Metrics
===================

.. automodule:: pyhard.metrics
   :members:
   :undoc-members:
   :show-inheritance:

Regression
===================

.. automodule:: pyhard.regression
   :members:
   :undoc-members:
   :show-inheritance:

Structures
===================

.. automodule:: pyhard.structures
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Utils
===================

.. automodule:: pyhard.utils
   :members:
   :no-undoc-members:
   :show-inheritance:
   :member-order: bysource

Validator
===================

.. automodule:: pyhard.validator
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

Visualization
===================

.. automodule:: pyhard.visualization
   :members:
   :undoc-members:
   :show-inheritance:
