import numpy as np

from pyhard.regression import Regressor, _reg_dict


def test_run_all(boston):
    regressor = Regressor(boston)
    result = regressor.run_all()

    assert len(result.columns) == len(_reg_dict.keys())
    assert not result.isna().any(axis=None)  # noqa
    assert len(boston) == len(result)


def test_score(boston):
    regressor = Regressor(boston)

    y_true = np.random.uniform(size=1000)
    y_pred = np.random.uniform(size=1000) + 10 * np.array(range(len(y_true)))
    assert y_true.shape == y_pred.shape

    s, p = regressor.score('absolute_error', y_true, y_pred)
    assert np.all(np.diff(s) >= 0)
    assert np.all(np.diff(p) <= 0)
    assert np.all(np.logical_and(p >= 0, p <= 1))
